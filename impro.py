#!/usr/bin/env python

# Python libs
import sys, time

# numpy and scipy
import numpy as np
from scipy.ndimage import filters

# OpenCV
import cv2

# Ros libraries
import roslib
import rospy

# Ros Messages
from sensor_msgs.msg import CompressedImage

# Image process
import imutils


from std_msgs.msg import Empty
from espeak import espeak

pub_square = None
pub_tri = None
squareFound = 0
triFound = 0

#################################################################################
#################################################################################
############################## SHAPE DETECTOR CLASS #############################
class ShapeDetector:
	def __init__(self):
		pass

	def detect(self, c):
		# initialize the shape name and approximate the contour
		shape = "unidentified"
		peri = cv2.arcLength(c, True)
		approx = cv2.approxPolyDP(c, 0.04 * peri, True)

		# if the shape is a triangle, it will have 3 vertices
		if len(approx) == 3:
			if cv2.contourArea(c) > 70 and cv2.contourArea(c) < 450 :
			# add aspect ration for triangle to remove weird 3 sided shapes
				#print 'Triangle Area: ', cv2.contourArea(c)
				(x, y, w, h) = cv2.boundingRect(approx)
				ar = w / float(h)
				

				shape = "Triangle" if ar >= 0.6 and ar <= 1.3 else "unidentified"

		# if the shape has 4 vertices, it is either a square or
		# a rectangle
		elif len(approx) == 4:
			if cv2.contourArea(c) > 300 and cv2.contourArea(c) < 800:
			# compute the bounding box of the contour and use the
			# bounding box to compute the aspect ratio
				#print 'Square Area: ',cv2.contourArea(c)
				(x, y, w, h) = cv2.boundingRect(approx)
				ar = w / float(h)

			# a square will have an aspect ratio that is approximately
			# equal to one, otherwise, the shape is a rectangle
				shape = "Square" if ar >= 0.9 and ar <= 1.1 else "rectangle"

	

		# return the name of the shape
		return shape
#################################################################################
#################################################################################
##############################     MAIN CLASS      ##############################

VERBOSE=False

class image_feature:

##############################     ROS STUFF      ##############################
    def __init__(self):
        '''Initialize ros publisher, ros subscriber'''
        # topic where we publish
        self.image_pub = rospy.Publisher("/output/image_raw/compressed",
            CompressedImage)
	self.pub_tri = rospy.Publisher('/drop/blue/pose', Empty, queue_size=10)
	self.pub_square = rospy.Publisher('/drop/red/pose', Empty, queue_size=10)
        # self.bridge = CvBridge()

        # subscribed Topic
        self.subscriber = rospy.Subscriber("/raspicam_node/image/compressed",
            CompressedImage, self.callback,  queue_size = 1)
        if VERBOSE :
            print "subscribed to /raspicam_node/image/compressed"


    def callback(self, ros_data):
        '''Callback function of subscribed topic. 
        Here images get converted and features detected'''
        if VERBOSE :
            print 'received image of type: "%s"' % ros_data.format

    	global squareFound
    	global triFound

        #### direct conversion to CV2 ####
        np_arr = np.fromstring(ros_data.data, np.uint8)
        #image_np = cv2.imdecode(np_arr, cv2.IMREAD_COLOR)
        image_np = cv2.imdecode(np_arr, cv2.IMREAD_COLOR) # OpenCV >= 3.0:
        
#################################################################################
#################################################################################
##############################  Image Processing code ###########################

	## Setting up square variables
		## These are used for shape size, centroid
		## location and number of squares found for
		## heirachy purposes

	squareCount = 0 #number of squares
	insideSquare = 0 #inside square area
	outsideSquareX = 0 #centroid
	outsideSquareY = 0 #centroid
	outsideSquare = 0 #area
	squareSameCentroid = False
	
	## Setting up triangle variables
	triCount = 0
	insideTri = 0
	outsideTri = 0
	outsideTriX = 0
	outsideTriY = 0	
	triSameCentroid = False

# Take every frame of stream
	frame = image_np
	resized = imutils.resize(frame, width=300) # resize for analysis
	ratio = frame.shape[0] / float(resized.shape[0]) # save ratio for later

# Convert to HSV
	hsv = cv2.cvtColor(resized, cv2.COLOR_BGR2HSV)


# Colour HSV range ###NEEDS BETTER TUNING###
	lower_orange = np.array([5,120,130 ])
	upper_orange = np.array([18,200,230 ])
	

# Blur the masked image
	blurred = cv2.GaussianBlur(hsv, (1, 1), 0) 

# Threshold 
	masked = cv2.inRange(blurred,lower_orange, upper_orange)



#################################################################

# find contours in the thresholded image and initialize the
# shape detector
	cnts = cv2.findContours(masked.copy(), cv2.RETR_CCOMP,
	cv2.CHAIN_APPROX_SIMPLE)
	cnts = cnts[0] if imutils.is_cv2() else cnts[1]
	sd = ShapeDetector()

# loop over the contours
	
	for c in cnts:
	# compute the center of the contour, then detect the name of the
	# shape using only the contour
		M = cv2.moments(c)
		
		# If no shape found, moment defaults to 0, therefore if
		# Statement acts like an error catch and defaults the 
		# centroids to zero zero if there is no shape
		if M["m00"] > 0:
			cX = int((M["m10"] / M["m00"]) * ratio)
			cY = int((M["m01"] / M["m00"]) * ratio)
		else :
			cX = 0
			cY = 0
		shape = sd.detect(c) # Determines name of shape 
		
		
		
	# multiply the contour (x, y)-coordinates by the resize ratio,
	# then draw the contours and the name of the shape on the image
		c = c.astype("float")
		c *= ratio
		c = c.astype("int")
		
	##### This is where false detection removal is initiated 

		if shape == str("Square"):
			# Due to heirachy big square is 1st and small square will be 2nd
			# As its inside the big one
			if squareCount == 0 and cv2.contourArea(c) > 350: # Also checks size
				outsideSquare = cv2.contourArea(c) # save area
				cv2.drawContours(frame, [c], -1, (0, 255,0), 2) # draw contours
				# Save the centroids
				outsideSquareX = cX
				outsideSquareY = cY
			# if a second square is found
			if squareCount == 1:
				insideSquare = cv2.contourArea(c) # save area
				# Check if same centroid
				if cX > (outsideSquareX - 50) and cX < (outsideSquareX + 50):
					if cY > (outsideSquareY - 50) and cY < (outsideSquareY + 50):
						squareSameCentroid = True # has same centroid 
				
			# If theres 2 squares, inside square is smaller than outside square and 
			# they have the same centroid we have found our shape
			if squareCount == 1 and insideSquare < outsideSquare and squareSameCentroid == True:
				cv2.drawContours(frame, [c], -1, (0, 255, 0), 2) # draw contours
				cv2.putText(frame, "TARGET FOUND! ", (cX - 100, cY + 50), cv2.FONT_HERSHEY_SIMPLEX,
					1, (0, 0,255), 4) # place text on screen
				cv2.putText(frame, "X " , (cX - 10, cY +10), cv2.FONT_HERSHEY_SIMPLEX,
					1, (0, 0, 255), 4) # place x on centroid

				# Print the centroid
				print("Square Image Coord: "+str(cX)+" "+str(cY))


				## Draw bounding box
				rect = cv2.minAreaRect(c)
				box = cv2.boxPoints(rect)
				box = np.int0(box)
				cv2.drawContours(frame,[box],0,(0,0,255),2)
				print("Square Boundingbox: "+str(box))

				# Save the data for solve pnp
				squarepoints = []
				cent = [cX, cY]
				squarepoints.append(cent)
				squarepoints = np.vstack((squarepoints,box))
				if squareFound == 0:
					self.pub_square.publish(Empty())
					squareFound =+ 1
					

				# Reset saved variables 
				squareCount = 0
				insideSquare = 0
				outsideSquare = 0
				# Reset saved centroids
				outsideSquareX = 0
				outsideSquareY = 0
				squareSameCentroid = False
			squareCount = squareCount + 1
#####################################################################

## Repeat for triangle
#########################################################################
	lower_blue = np.array([110,50,50 ])
	upper_blue = np.array([135,200,200 ])

# Threshold 
	maskedb = cv2.inRange(hsv,lower_blue, upper_blue)


# find contours in the thresholded image and initialize the
# shape detector
	cnts = cv2.findContours(maskedb.copy(), cv2.RETR_CCOMP,
	cv2.CHAIN_APPROX_SIMPLE)
	cnts = cnts[0] if imutils.is_cv2() else cnts[1]
	sd = ShapeDetector()

# loop over the contours
	
	for c in cnts:
	# compute the center of the contour, then detect the name of the
	# shape using only the contour
		M = cv2.moments(c)
		
		
		if M["m00"] > 0:
			cX = int((M["m10"] / M["m00"]) * ratio)
			cY = int((M["m01"] / M["m00"]) * ratio)
		else :
			cX = 0
			cY = 0
		shape = sd.detect(c)
		
		
		
	# multiply the contour (x, y)-coordinates by the resize ratio,
	# then draw the contours and the name of the shape on the image
		c = c.astype("float")
		c *= ratio
		c = c.astype("int")
		
		if shape == str("Triangle"):
			
			if triCount == 0 and cv2.contourArea(c) > 280:
				outsideTri = cv2.contourArea(c)
				cv2.drawContours(frame, [c], -1, (0, 255,0), 2)
				outsideTriX = cX
				outsideTriY = cY

			if triCount == 1:
				insideTri = cv2.contourArea(c)
				if cX > (outsideTriX - 50) and cX < (outsideTriX + 50):
					if cY > (outsideTriY - 50) and cY < (outsideTriY + 50):
						triSameCentroid = True 

			if triCount == 1 and insideTri < outsideTri and triSameCentroid == True:
				cv2.drawContours(frame, [c], -1, (0, 255, 0), 2)
				cv2.putText(frame, "TARGET FOUND! ", (cX - 100, cY + 50), cv2.FONT_HERSHEY_SIMPLEX,
					1, (0, 0,255), 4)
				cv2.putText(frame, "X " , (cX - 10, cY +10), cv2.FONT_HERSHEY_SIMPLEX,
					1, (0, 0, 255), 4)
				print("Triangle Image Coord: "+str(cX)+" "+str(cY))
				
				## Draw bounding box
				rect = cv2.minAreaRect(c)
				box = cv2.boxPoints(rect)
				box = np.int0(box)
				cv2.drawContours(frame,[box],0,(0,0,255),2)
				
				# SAve points for solvepnp
				tripoints = []
				tricent = [cX, cY]
				tripoints.append(tricent)
				tripoints = np.vstack((tripoints,box))
				if triFound == 0:
					self.pub_tri.publish(Empty())
					triFound =+ 1
					
				triCount = 0
				insideTri = 0
				outsideTri = 0
				outsideTriX = 0
				outsideTriY = 0				
				triSameCentroid = False
			triCount = triCount + 1
       

#################################################################################
#################################################################################

        #### Create CompressedIamge ####
        msg = CompressedImage()
        msg.header.stamp = rospy.Time.now()
        msg.format = "jpeg"
        msg.data = np.array(cv2.imencode('.jpg', frame)[1]).tostring()
        # Publish new image
        self.image_pub.publish(msg)
        
        #self.subscriber.unregister()

def main(args):
    '''Initializes and cleanup ros node'''
    ic = image_feature()
    rospy.init_node('image_feature', anonymous=True)
    try:
        rospy.spin()
    except KeyboardInterrupt:
        print "Shutting down ROS Image feature detector module"
    cv2.destroyAllWindows()

if __name__ == '__main__':
    main(sys.argv)