#!/usr/bin/env python

# Python libs
import sys, time, math

# numpy and scipy
import numpy as np
from scipy.ndimage import filters

# OpenCV
import cv2

# Ros libraries
import roslib
import rospy

# Ros Messages
from sensor_msgs.msg import CompressedImage
from sensor_msgs.msg import Image
from sensor_msgs.msg import CameraInfo
from std_msgs.msg import Time

from cv_bridge import CvBridge, CvBridgeError

# Image process
import imutils

# Localisation
import tf2_ros
import tf_conversions
from geometry_msgs.msg import TransformStamped

from espeak import espeak
import time



# Global Variables
squareFound = 0
triFound = 0

tfsub = None

tfbr = None
pub_square_found = None
pub_tri_found = None

uav_name = "groupFour"
camera_name = "raspiCam"
target_name = "target"





#################################################################################
#################################################################################
############################## SHAPE DETECTOR CLASS #############################
class ShapeDetector:
       

	def __init__(self):
                pass

        def detect(self, c):
                # initialize the shape name and approximate the contour
                shape = "unidentified"
                peri = cv2.arcLength(c, True)
                approx = cv2.approxPolyDP(c, 0.04 * peri, True)

                # if the shape is a triangle, it will have 3 vertices
                if len(approx) == 3:
                        if cv2.contourArea(c) > 70 and cv2.contourArea(c) < 450 :
                                # add aspect ration for triangle to remove weird 3 sided shapes
                                #print 'Triangle Area: ', cv2.contourArea(c)
                                (x, y, w, h) = cv2.boundingRect(approx)
                                ar = w / float(h)


                                shape = "Triangle" if ar >= 0.6 and ar <= 1.3 else "unidentified"

                # if the shape has 4 vertices, it is either a square or
                # a rectangle
                elif len(approx) == 4:
                        if cv2.contourArea(c) > 300 and cv2.contourArea(c) < 800:
                        # compute the bounding box of the contour and use the
                        # bounding box to compute the aspect ratio
                                #print 'Square Area: ',cv2.contourArea(c)
                                (x, y, w, h) = cv2.boundingRect(approx)
                                ar = w / float(h)

                        # a square will have an aspect ratio that is approximately
                        # equal to one, otherwise, the shape is a rectangle
                                shape = "Square" if ar >= 0.9 and ar <= 1.1 else "rectangle"



                # return the name of the shape
                return shape
#################################################################################
#################################################################################
##############################     MAIN CLASS      ##############################

VERBOSE=False

class image_feature:

	# Global Variables
    global tfbr
    global tfsub
    global pub_square_found
    global pub_tri_found 

    global uav_name
    global camera_name 
    global target_name

    global squareFound
    global triFound
 
##############################     ROS STUFF      ##############################
    def __init__(self):
        '''Initialize ros publisher, ros subscriber'''
        # topic where we publish
        self.image_pub = rospy.Publisher("/output/image_raw/compressed",
            CompressedImage)
        # self.bridge = CvBridge()
        
        # Publish Square Topic for Nav.
        self.target_pub_square = rospy.Publisher("/output/Square-Found", Time)

        # Publish Triangle Topic for Nav.
        self.target_pub_tri = rospy.Publisher("/output/Tri-Found", Time)

        # Subscribe to Camera Info.
        #self.sub_info = rospy.Subscriber("~camera_info", CameraInfo, self.callback_info)

        # Set up tf2.
        self.tfbr = tf2_ros.TransformBroadcaster()

        # Camera Matrix.
        self.camera_matrix = np.array([(680.374408, 0.000, 322.117639),
	         		(0.000, 682.825147, 192.007517),
	         		(0.000, 0.000, 1.000)],dtype=np.float32)

        # Distortion Coefficients. 
        self.dist_coeffs = np.array([(-0.085651, 0.133490, -0.010264, 0.004822, 0.000)],dtype=np.float32)

        # subscribed Topic
        self.subscriber = rospy.Subscriber("/raspicam_node/image/compressed",
            CompressedImage, self.callback,  queue_size = 1)
        if VERBOSE :
            print "subscribed to /raspicam_node/image/compressed"

        
    def callback(self, ros_data):
        '''Callback function of subscribed topic.
        Here images get converted and features detected'''
        if VERBOSE :
            print 'received image of type: "%s"' % ros_data.format

        #### direct conversion to CV2 ####
        np_arr = np.fromstring(ros_data.data, np.uint8)
        #image_np = cv2.imdecode(np_arr, cv2.IMREAD_COLOR)
        image_np = cv2.imdecode(np_arr, cv2.IMREAD_COLOR) # OpenCV >= 3.0:




	def send_Square_target(tvec):
		# Generate the "found" timestamp
		time_found = rospy.Time.now()

		# Create an arbitrary transform in the camera frame.
		squareTF = TransformStamped()
		squareTF.header.stamp = time_found
		squareTF.header.frame_id = camera_name
		squareTF.header.child_frame_id = target_name

		squareTF.transform.translation.x = tvec[0]
		squareTF.transform.translation.y = tvec[1]
		squareTF.transform.translation.z = tvec[2]
		squareTF.transform.rotation.x = 0.0
		squareTF.transform.rotation.y = 0.0
		squareTF.transform.rotation.z = 0.0
		squareTF.transform.rotation.w = 1.0

		# Send the transformation to TF and "found" timestamp to localiser.
		tfbr.sendTransform(squareTF)
		pub_square_found.publish(time_found)

		tfbr = None



	def send_Triangle_target(tvec):
		# Generate the "found" timestamp
		time_found = rospy.Time.now()

		# Create an arbitrary transform in the camera frame.
		triTF = TransformStamped()
		triTF.header.stamp = time_found
		triTF.header.frame_id = camera_name
		triTF.header.child_frame_id = target_name

		triTF.transform.translation.x = tvec[0]
		triTF.transform.translation.y = tvec[1]
		triTF.transform.translation.z = tvec[2]
		triTF.transform.rotation.x = 0.0
		triTF.transform.rotation.y = 0.0
		triTF.transform.rotation.z = 0.0
		triTF.transform.rotation.w = 1.0

		# Send the transformation to TF and "found" timestamp to localiser.
		tfbr.sendTransform(triTF)
		pub_tri_found.publish(time_found)

		tfbr = None




	def send_cam_tf():
		# Create a static transform to represent the 
		# camera's position/orientation relative to the uav.
	
		camTransform = TransformStamped()
		camTransform.header.stamp = rospy.Time.now()
		camTransform.header.frame_id = uav_name
		camTransform.child_frame_id = camera_name

		# Transform the cam's position.
		camTransform.transform.translation.x = - 0.104	# Measure Cam on UAV!
		camTransform.transform.translation.y = 0.0
		camTransform.transform.translation.z = 0.130

		# Transform the cam's orientation.
		q = tf_conversions.transformations.quaternion_from_euler(0, math.pi, 0)
		camTransform.transform.rotation.x = q[0]
		camTransform.transform.rotation.y = q[1]
		camTransform.transform.rotation.z = q[2]
		camTransform.transform.rotation.w = q[3]

		# Send the static transform.
		tfsub.sendTransform(camTransform)


#################################################################################
#################################################################################
##############################  Image Processing code ###########################

        ## Setting up square variables
                ## These are used for shape size, centroid
                ## location and number of squares found for
                ## heirachy purposes
	global triFound
	global squareFound

        squareCount = 0 #number of squares
        insideSquare = 0 #inside square area
        outsideSquareX = 0 #centroid
        outsideSquareY = 0 #centroid
        outsideSquare = 0 #area
        squareSameCentroid = False

        ## Setting up triangle variables
        triCount = 0
        insideTri = 0
        outsideTri = 0
        outsideTriX = 0
        outsideTriY = 0
        triSameCentroid = False

# Take every frame of stream
        frame = image_np
        resized = imutils.resize(frame, width=300) # resize for analysis
        ratio = frame.shape[0] / float(resized.shape[0]) # save ratio for later

# Convert to HSV
        hsv = cv2.cvtColor(resized, cv2.COLOR_BGR2HSV)


# Colour HSV range ###NEEDS BETTER TUNING###
        lower_orange = np.array([5,120,130 ])
        upper_orange = np.array([18,200,230 ])


# Blur the masked image
        blurred = cv2.GaussianBlur(hsv, (1, 1), 0)

# Threshold
        masked = cv2.inRange(blurred,lower_orange, upper_orange)



#################################################################

# find contours in the thresholded image and initialize the
# shape detector
        cnts = cv2.findContours(masked.copy(), cv2.RETR_CCOMP,
        cv2.CHAIN_APPROX_SIMPLE)
        cnts = cnts[0] if imutils.is_cv2() else cnts[1]
        sd = ShapeDetector()

# loop over the contours

        for c in cnts:
        # compute the center of the contour, then detect the name of the
        # shape using only the contour
                M = cv2.moments(c)

                # If no shape found, moment defaults to 0, therefore if
                # Statement acts like an error catch and defaults the
                # centroids to zero zero if there is no shape
                if M["m00"] > 0:
                        cX = int((M["m10"] / M["m00"]) * ratio)
                        cY = int((M["m01"] / M["m00"]) * ratio)
                else :
                        cX = 0
                        cY = 0
                shape = sd.detect(c) # Determines name of shape



        # multiply the contour (x, y)-coordinates by the resize ratio,
        # then draw the contours and the name of the shape on the image
                c = c.astype("float")
                c *= ratio
                c = c.astype("int")

        ##### This is where false detection removal is initiated

                if shape == str("Square"):
                        
                        # Define a model of the square target for the pose solver.
                        r = 0.0625  #0.0975
                        self.model_object = np.array([(0.0, 0.0, 0.0),
                                                        (r, r, 0.0),
                                                        (r, -r, 0.0),
                                                        (-r, r, 0.0),
                                                        (-r, -r, 0.0)],dtype=np.float32)

                        # Due to heirachy big square is 1st and small square will be 2nd
                        # As its inside the big one
                        if squareCount == 0 and cv2.contourArea(c) > 350: # Also checks size
                                outsideSquare = cv2.contourArea(c) # save area
                                cv2.drawContours(frame, [c], -1, (0, 255,0), 2) # draw contours
                                # Save the centroids
                                outsideSquareX = cX
                                outsideSquareY = cY
                        # if a second square is found
                        if squareCount == 1:
                                insideSquare = cv2.contourArea(c) # save area
                                # Check if same centroid
                                if cX > (outsideSquareX - 50) and cX < (outsideSquareX + 50):
                                        if cY > (outsideSquareY - 50) and cY < (outsideSquareY + 50):
                                                squareSameCentroid = True # has same centroid

                        # If theres 2 squares, inside square is smaller than outside square and
                        # they have the same centroid we have found our shape
                        if squareCount == 1 and insideSquare < outsideSquare and squareSameCentroid == True and squareFound ==0:
                                cv2.drawContours(frame, [c], -1, (0, 255, 0), 2) # draw contours
                                cv2.putText(frame, "TARGET FOUND! ", (cX - 100, cY + 50), cv2.FONT_HERSHEY_SIMPLEX,
                                        1, (0, 0,255), 4) # place text on screen
                                cv2.putText(frame, "X " , (cX - 10, cY +10), cv2.FONT_HERSHEY_SIMPLEX,
                                        1, (0, 0, 255), 4) # place x on centroid

                                # Print the centroid
                                print("Square Image Coord: "+str(cX)+" "+str(cY))


                                ## Draw bounding box
                                rect = cv2.minAreaRect(c)
                                box = cv2.boxPoints(rect)
                                box = np.float32(box)
                                cv2.drawContours(frame,[box],0,(0,0,255),2)
                                print("Square Boundingbox: "+str(box))

                                # Define a model of the object in the Image.
                                squarepoints = []
                                cent = [cX, cY]
                                squarepoints.append(cent)
                                squarepoints = np.vstack((squarepoints,box)).astype(np.float32)
				
				# vocalise
				espeak.synth("Orange Square Identified Fools")

                                # SolvePnP().
				(success, rvec, tvec) = cv2.solvePnP(self.model_object, squarepoints, self.camera_matrix, self.dist_coeffs)
				squareFound += 1

				if success:
					# Send Static Transform. 
					tfsub = tf2_ros.StaticTransformBroadcaster()
					send_cam_tf()
					rospy.loginfo("Broadcasting Cam TF.")

					# Setup tf2 broadcaster.
					tfbr = tf2_ros.TransformBroadcaster()

					# Setup timestamp publisher.
					pub_square_found = rospy.Publisher('/drop/red/pose', TransformStamped, queue_size = 10)

					# Wait for node to configure.
					rospy.sleep(rospy.Duration(2))

					# Send target msg.
					send_Square_target( tvec )

					# Wait for node to transmit.
					rospy.sleep(rospy.Duration(2))
					rospy.loginfo("Square Target TF and Timestamp: SENT.")



			


                                # Reset saved variables
                                squareCount = 0
                                insideSquare = 0
                                outsideSquare = 0
                                # Reset saved centroids
                                outsideSquareX = 0
                                outsideSquareY = 0
                                squareSameCentroid = False
                        squareCount = squareCount + 1
#####################################################################

## Repeat for triangle
#########################################################################
        lower_blue = np.array([110,50,50 ])
        upper_blue = np.array([135,200,200 ])

# Threshold
        maskedb = cv2.inRange(hsv,lower_blue, upper_blue)


# find contours in the thresholded image and initialize the
# shape detector
        cnts = cv2.findContours(maskedb.copy(), cv2.RETR_CCOMP,
        cv2.CHAIN_APPROX_SIMPLE)
        cnts = cnts[0] if imutils.is_cv2() else cnts[1]
        sd = ShapeDetector()

# loop over the contours

        for c in cnts:
        # compute the center of the contour, then detect the name of the
        # shape using only the contour
                M = cv2.moments(c)


                if M["m00"] > 0:
                        cX = int((M["m10"] / M["m00"]) * ratio)
                        cY = int((M["m01"] / M["m00"]) * ratio)
                else :
                        cX = 0
                        cY = 0
                shape = sd.detect(c)



        # multiply the contour (x, y)-coordinates by the resize ratio,
        # then draw the contours and the name of the shape on the image
                c = c.astype("float")
                c *= ratio
                c = c.astype("int")

                if shape == str("Triangle"):
			r = 0.0525 
			self.model_object = np.array([(0.0,0.0,0.0),
						(r,r,0.0),
						(r,-r,0.0),
						(-r,r,0.0),
						(-r,-r,0.0)],dtype=np.float32)

                        if triCount == 0 and cv2.contourArea(c) > 280:
                                outsideTri = cv2.contourArea(c)
                                cv2.drawContours(frame, [c], -1, (0, 255,0), 2)
                                outsideTriX = cX
                                outsideTriY = cY

                        if triCount == 1:
                                insideTri = cv2.contourArea(c)
                                if cX > (outsideTriX - 50) and cX < (outsideTriX + 50):
                                        if cY > (outsideTriY - 50) and cY < (outsideTriY + 50):
                                                triSameCentroid = True

                        if triCount == 1 and insideTri < outsideTri and triSameCentroid == True and triFound == 0:
                                cv2.drawContours(frame, [c], -1, (0, 255, 0), 2)
                                cv2.putText(frame, "TARGET FOUND! ", (cX - 100, cY + 50), cv2.FONT_HERSHEY_SIMPLEX,
                                        1, (0, 0,255), 4)
                                cv2.putText(frame, "X " , (cX - 10, cY +10), cv2.FONT_HERSHEY_SIMPLEX,
                                        1, (0, 0, 255), 4)
                                print("Triangle Image Coord: "+str(cX)+" "+str(cY))

                                ## Draw bounding box
                                rect = cv2.minAreaRect(c)
                                box = cv2.boxPoints(rect)
                                box = np.int0(box)
                                cv2.drawContours(frame,[box],0,(0,0,255),2)

                                # Save points for solvepnp
                                tripoints = []
                                tricent = [cX, cY]
                                tripoints.append(tricent)
                                tripoints = np.vstack((tripoints,box)).astype(np.float32)
				# vocalise
				espeak.synth("Blue Triangle Identified Fools")

				# SolvePnP().
				(success, rvec, tvec) = cv2.solvePnP(self.model_object, tripoints, self.camera_matrix, self.dist_coeffs)

				triFound += 1
				if success:
					# Send Static Transform. 
					tfsub = tf2_ros.StaticTransformBroadcaster()
					send_cam_tf()
					rospy.loginfo("Broadcasting Cam TF.")

					# Setup tf2 broadcaster.
					tfbr = tf2_ros.TransformBroadcaster()

					# Setup timestamp publisher.
					pub_tri_found = rospy.Publisher('/drop/blue/pose', TransformStamped, queue_size = 10)

					# Wait for node to configure.
					rospy.sleep(rospy.Duration(2))

					# Send target msg.
					send_Triangle_target( tvec )

					# Wait for node to transmit.
					rospy.sleep(rospy.Duration(2))
					rospy.loginfo("Triangle Target TF and Timestamp: SENT.")


                                triCount = 0
                                insideTri = 0
                                outsideTri = 0
                                outsideTriX = 0
                                outsideTriY = 0
                                triSameCentroid = False
                        triCount = triCount + 1


#################################################################################
#################################################################################

        #### Create CompressedIamge ####
        msg = CompressedImage()
        msg.header.stamp = rospy.Time.now()
        msg.format = "jpeg"
        msg.data = np.array(cv2.imencode('.jpg', frame)[1]).tostring()
        # Publish new image
        self.image_pub.publish(msg)

        #self.subscriber.unregister()

def main(args):
    '''Initializes and cleanup ros node'''
    ic = image_feature()
    rospy.init_node('image_feature', anonymous=True)
    try:
        rospy.spin()
    except KeyboardInterrupt:
        print "Shutting down ROS Image feature detector module"
    cv2.destroyAllWindows()

if __name__ == '__main__':
    main(sys.argv)
